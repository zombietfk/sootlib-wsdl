<?php

namespace Sootlib\WSDL;

use SoapServer;
use SoapClient;
use SoapFault;

class WSDLDefinedWebService {

    private $soap_server;

    public $wsdl;
    /**
     * quinyx constructor
     * @param $wdsl_uri string - the address of the quinyx server
     */
    public function __construct($wsdl) {
        $this->wsdl = $wsdl;
        $this->soap_server = new SoapServer($wsdl);
    }

    public function __call($name, $args){
		try {
                        /*
                          sometimes it doesn't recongnize a
                          function name even if it exists.
                          I'm surpressing the warninig here
                          since it clutters the cli output too much, but
                          afaik it should be ok
                        */
                        @$this->soap_server->addFunction($name);
                        $soap_client = new SoapClient($this->wsdl);
                        $result = $soap_client->__soapCall($name, $args);
			return $result;
		} catch(SoapFault $e){
			echo json_encode(array(
				"error"=>$e->getMessage()
			));
			die;

		}
    }
}
